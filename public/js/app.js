// Initialize Firebase
var firebaseConfig = {
    apiKey: "AIzaSyAgZI324e7X8thm7Cd-8F4ucgqauC3thB8",
    authDomain: "queue-manager-dashboard.firebaseapp.com",
    databaseURL: "https://queue-manager-dashboard.firebaseio.com",
    projectId: "queue-manager-dashboard",
    storageBucket: "queue-manager-dashboard.appspot.com",
    messagingSenderId: "4818878846",
    appId: "1:4818878846:web:0017dc67e8fe4c8bc9732f",
    measurementId: "G-0594ZSD6MY"
  };
firebase.initializeApp(firebaseConfig);

const dbRef = firebase.database().ref();
const usersRef = dbRef.child('tickets');

// adding ticket to the list of tickets
usersRef.on("child_added", snap => {
  
    let user = snap.val();
  
    html = '<tbody><tr class="tr-shadow"><td><label class="au-checkbox"><input type="checkbox"><span class="au-checkmark"></span></label></td><td>%name%</td><td><a href="https://forgerock.zendesk.com/agent/tickets/%ticket%">https://forgerock.zendesk.com/agent/tickets/%url%</a></td><td>%date%</td><td><div class="table-data-feature"><button class="item" data-toggle="tooltip" data-placement="top" title="Edit"><i class="zmdi zmdi-edit"></i></button><button class="item delete-btn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="zmdi zmdi-delete"></i></button><button class="item" data-toggle="tooltip" data-placement="top" title="More"><i class="zmdi zmdi-more"></i></button></div></td></tr><tr class="spacer"></tr>';

    newHtml = html.replace('%name%', user.name);
    newHtml = newHtml.replace('%ticket%', user.ticket);
    newHtml = newHtml.replace('%url%', user.ticket);
    newHtml = newHtml.replace('%date%', user.date);

    document.querySelector("#ticket-table").insertAdjacentHTML('beforeend', newHtml);
  
});  

// UI CONTROLLER
var UIController = (function(){

    return{
        getinput: function() {
            return{
                name: document.querySelector("#user-name").value, // Will be either inc or exp
                ticket: document.querySelector("#ticket-number").value,
                date: document.querySelector("#ticket-date").value
            }
        }
    };

})();

var signOut = function(){
    firebase.auth().signOut().then(function() {
        console.log('You are signed out')
      }).catch(function(error) {
        console.log(error)
      });
};
// GLOBAL APP CONTROLLER
var controller = (function(UICtrl){

    var field = document.querySelector('#ticket-date');
    var date = new Date();
    field.value = date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString().padStart(2, 0) + 
    '-' + date.getDate().toString().padStart(2, 0);



    var setupEventListeners = function(){
        document.querySelector(".submit-btn").addEventListener('click', ctrlAddItem);
    };
    

    var ctrlAddItem = function(){
        var input
        input = UICtrl.getinput();

        usersRef.push(input, function () {

            console.log("data has been inserted");
        })
        
        console.log(input);

    
    };

    return {
        init: function() {
            setupEventListeners();
            
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    // User is signed in.
                    document.querySelector("#username").textContent = "Hello, "+firebase.auth().currentUser.email;
                    console.log(firebase.auth().currentUser.email);
                }
            });
        }
    }
    
})(UIController);

controller.init();